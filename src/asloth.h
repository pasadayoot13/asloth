!Compiler flag options:
!You can comment in/out desired compiler flag options that change the behaviour/physics of A-SLOTH


!Switches on additional text outputs while running
#define INFO

!Follow individual chemical elements
#define METAL_YIELDS

!Metallicity Distribution Function; tracks stellar masses as a function of [Fe/H] down to z=0
#define MDF

!Output volume fractions based on random sampling of volume. Works best for rectangular boxes.
#define VOL_FRACS

!Run without spatial information. Necessary for EPS-based trees. Not recommended if spatial information is available
#define NO_XYZ

!Using NBODY trees (e.g. Caterpillar) instead of statistical EPS trees
!#define NBODY

!Outputs baryon-information during sub-cycling along main branch. NextOutputID needs to be set if NBODY
!#define OUTPUT_GAS_BRANCH

!Shows more debugging information
!#define DEBUG

!Remove unnecessary columns of tree file and write smaller version. Only useful with NBODY
!#define CREATE_SMALL_TREE_FILE

!Output information of satellite galaxies
!#define Satellites

!Have only one IMF mass bin between M_SURVIVE and M_ION to save computational time and memory
!#define COMPACT_IMF
