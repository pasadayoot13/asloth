#include "asloth.h"
module Tree_Memory_Arrays
  ! This module is use for managing the memory of the merger tree
  use Defined_Types
  type (TreeNode), allocatable, target :: MergerTree_Aux(:)
end module Tree_Memory_Arrays

module Tree_Memory_Arrays_Passable
  ! This module is used to reference the merger tree
  use Defined_Types
  type (TreeNode), pointer :: MergerTree(:)
  integer(kind=selected_int_kind(8)) :: number_of_nodes
contains

  pure integer function Tree_Index(Node)
    implicit none
    type (TreeNode), pointer :: Node
    !
    ! Code
    Tree_Index = Node%id
    return
  end function Tree_Index

end module Tree_Memory_Arrays_Passable
