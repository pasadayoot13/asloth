#include "asloth.h"
module Defined_Types
  use metals, only: N_ELEMENTS
  use utility

  implicit none
  type TreeNode
    ! class for storing information of each halo at each time
     real :: mhalo !mass of halo
     integer :: jlevel !which time-step is the halo at
     integer :: nchild !number of child nodes
     integer :: id ! halo id
     logical :: i_mcrit = .False. !is the halo above the critical mass?
     logical :: i_ion = .False. !is the halo affected by external ionising radiation
     logical :: i_sub = .False. !is this a subhalo?
     logical :: i_enr = .False. ! is this halo enriched?
     logical :: i_SF = .False. ! this flag controls whether the SF_step subroutine is called
     real :: r_en = 0 !enrichment radius [physical Mpc]
     real :: R_ion = 0 ! ionised radius [physical Mpc]
     type (TreeNode), pointer :: child=>null(),parent=>null(),sibling=>null()
     type (TreeNode), pointer :: base ! which halo will this one be at at the the lowest redshift
     real :: M_peak ! peak mass [Msun]

#ifdef FILTER_MVIR
     real :: M_filter=0
     real :: M_peak_filter=0
#endif
#ifdef METAL_YIELDS
     real, dimension(:,:), allocatable :: m_metals
     !m_metals(1,:) int_III
     !m_metals(2,:) ext_III
     !m_metals(3,:) int_II
     !m_metals(4,:) ext_II
     !m_metals(5,:) out
     real, allocatable, dimension(:) :: IGM_Z !ambient metallicity
#endif

#ifdef NO_XYZ
     logical :: i_checked !has this halo already been checked for stochastic feedback?
#endif

#ifdef NBODY
#ifndef NO_XYZ
     real :: x(3) ! position
#endif
     integer :: lp_id ! last progenitor depthfirst id
#ifdef DEBUG
     integer :: desc_id ! descendent id
#endif

#endif
     real :: M_starII ! stellar mass Pop II [M_sun]
     real :: M_starIII ! stellar mass Pop III [M_sun]
     real :: M_hot ! hot gas mass [M_sun]
     real :: M_cold ! cold gas mass [M_sun]
     real :: M_out ! outflow mass [M_sun]
     real :: L_ion ! ionising radiation emission rate [Photons/second]
     integer :: pop

#if (defined(MDF))
     !Stores allocatable arrays of a Node
     !As pointer because it takes up memory and only very few nodes need this information
     !Necessary because parallel loop does not allow to write to shared arrays
     type(Node_Arrays), pointer :: This_Array => null()
#endif

     integer :: num_PopIIProgenitor = 0 ! how many Pop II forming progenitors
     logical :: firstPopIISF = .True.
     real :: M_starII_surv = 0.! Msun
     type(Stars_HighMass), pointer :: HighStars => null()
     type(Stars_HighMass), pointer :: HighStars_first => null()
     type(Stars_HighMass), pointer :: HighStars_last  => null()
#ifdef Satellites
     type(Stars_HighMass), pointer :: LLStars => null()
     type(Stars_HighMass), pointer :: LLStars_first => null()
     type(Stars_HighMass), pointer :: LLStars_last  => null()
#endif

   contains
     procedure, pass(this) :: set_baryons
     procedure, pass(this) :: add_baryons

   end type TreeNode

  type Node_Arrays!Always present and contains allocatable arrays
#ifdef MDF
    !first dim: [Fe/H]
    !second dim: different subsets:
    !1: all stars
    !2: CEMP
    !3: PopII/PopIII fractions?
    !4: external/internal enrichment?
    !one could also trace the stellar mass here (in addition to number of stars)
    real, dimension (:,:), allocatable :: MDF_array
#endif
  end type Node_Arrays



  type Stars_HighMass
    type(Stars_HighMass), pointer :: prev => null() ! previous star in node
    type(Stars_HighMass), pointer :: next => null() ! next star in node

    integer :: num = 0
    integer :: bin_id = 0  ! which bin it belongs, should be consistent with IMF arrays

    integer :: pop = 0 ! PopII (2) or PopIII (3) stars
    !pop and Z (below) should be causally connected.
    !However, better be explicit if someone wants to include another definition of the populations

    real :: timeborn  = 0. ! in order to know when the stars die 
    real, dimension(:), allocatable :: Z ! Zgas [Z/Zsun] when the star is born. Related with SN yields.
    contains
      final :: del_high_mass
  end type Stars_HighMass
  interface Stars_HighMass
    procedure :: make_stars_high_mass
  end interface
contains
 
  pure function make_stars_high_mass(n, i, timeborn, Zgas, pop) result(res)
      type(Stars_HighMass) :: res
      integer, intent(in) :: n, i, pop
      real, intent(in) :: timeborn
      real, dimension(:), intent(in) :: Zgas
      nullify(res%prev)
      nullify(res%next)
      res%num = n
      res%bin_id = i ! such that we can easily get the lifetime/SNe energy, etc. from predetermined arrays 
      res%timeborn = timeborn ! time of star formation in seconds
      allocate(res%Z(N_ELEMENTS))
      res%Z(:)   = Zgas(:) ! stellar metallicity that adopts from the gas metallicity. no elemental information yet.
      res%pop = pop
  end function make_stars_high_mass
  
  pure subroutine del_high_mass(this)
      type(Stars_HighMass), intent(inout) :: this
      if (allocated(this%Z)) deallocate(this%Z)
      nullify(this%prev)
      nullify(this%next)
  end subroutine

  subroutine set_baryons(this, other)
    !copies baryonic properties from one halo to another (overwrites)
    class(TreeNode) :: this
    class(TreeNode) :: other

    this%M_hot = other%M_hot
    this%M_starII = other%M_starII
    this%M_starIII = other%M_starIII
    this%M_cold = other%M_cold
    this%M_out = other%M_out

    this%i_enr = other%i_enr

#ifdef METAL_YIELDS
    if (.not. allocated(this%m_metals)) then
      allocate(this%m_metals(5,N_ELEMENTS))
      this%m_metals(:,:) = 0.0
    endif
    if (.not. allocated(other%m_metals)) then
      allocate(other%m_metals(5,N_ELEMENTS))
      other%m_metals(:,:) = 0.0
    endif
    this%m_metals = other%m_metals
#endif
  end subroutine


  subroutine add_baryons(this, other)
    !adds baryonic properties of one halo to another
    class(TreeNode) :: this
    class(TreeNode) :: other

    this%M_hot = this%M_hot + other%M_hot
    this%M_starII  = this%M_starII  + other%M_starII
    this%M_starIII = this%M_starIII + other%M_starIII
    this%M_cold = this%M_cold + other%M_cold
    this%M_out = this%M_out + other%M_out

    this%i_enr = (this%i_enr .or.  other%i_enr)

#ifdef METAL_YIELDS
    if (.not. allocated(other%m_metals)) then
      allocate(other%m_metals(5,N_ELEMENTS))
      other%m_metals(:,:) = 0.0
    endif
    this%m_metals = this%m_metals + other%m_metals
#endif
  end subroutine



  subroutine print_all_props(a)
    ! Printing function that prints various porperties of a halo in order to allow
    ! easier tracing of errors and unexpected changes
     type(TreeNode), intent(in) :: a
     type(TreeNode), pointer :: Dummy_Node
     write(*,*) "===== writing node Nr. ", a%id, "====="
     write(*,*) "mhalo           =", a%mhalo            
     write(*,*) "jlevel          =", a%jlevel           
     write(*,*) "nchild          =", a%nchild           
#ifdef NBODY
#ifndef NO_XYZ
     write(*,*) "x               =", a%x
#endif
     write(*,*) "r_en            =", a%r_en
     write(*,*) "i_sub           =", a%i_sub 
#endif     
     write(*,*) "i_mcrit         =", a%i_mcrit          
     write(*,*) "i_SF            =", a%i_SF      
     write(*,*) "i_ion           =", a%i_ion   
     write(*,*) "R_ion           =", a%R_ion   
     write(*,*) "M_hot           =", a%M_hot            
     write(*,*) "M_cold          =", a%M_cold           
     write(*,*) "M_out           =", a%M_out            
     write(*,*) "L_ion           =", a%L_ion            
     write(*,*) "Mstar_PopII     =", a%M_starII
     write(*,*) "Mstar_PopIII    =", a%M_starIII
#ifdef METAL_YIELDS
     if (allocated(a%m_metals)) then
       write(*,*) "m_metals(int_III)=", a%m_metals(1,:)
       write(*,*) "m_metals(ext_III)=", a%m_metals(2,:)
       write(*,*) "m_metals(int_II) =", a%m_metals(3,:)
       write(*,*) "m_metals(ext_II) =", a%m_metals(4,:)
       write(*,*) "m_metals(out)    =", a%m_metals(5,:)
     else
       write(*,*) "m_metals(:,:)    = 0"
     endif
     write(*,*) "IGM_Z              =", a%IGM_Z               
#endif
     write(*,*) "child nodes:" 
    if (associated(a%child)) then
      Dummy_Node => a%child
      write(*,*) "child ID        ="  ,  Dummy_Node%ID 
      do while(associated(Dummy_Node%sibling))
        Dummy_Node => Dummy_Node%sibling
        write(*,*) "child ID        ="  ,  Dummy_Node%ID 
      end do
    endif 
    if (associated(a%parent)) then
      Dummy_Node => a%parent
      write(*,*) "parent ID       ="  ,  Dummy_Node%ID 
    endif
 end subroutine
end module Defined_Types
