dB17.dat: https://ui.adsabs.harvard.edu/abs/2017MNRAS.465..926D/abstract, Fig.4
Graziani15.dat: https://ui.adsabs.harvard.edu/abs/2015MNRAS.449.3137G/abstract, Fig.2
Robertson15.dat: https://ui.adsabs.harvard.edu/abs/2015ApJ...802L..19R/abstract, Fig.3, "ML+68%"
Salvadori14.dat: https://ui.adsabs.harvard.edu/abs/2014MNRAS.437L..26S/abstract, Fig.2, C=3

References to further observational points are provided in the script
They are based on:
#Masen+18, Fig. 12 https://ui.adsabs.harvard.edu/abs/2018ApJ...856....2M/abstract
#Finkelstein+19, Fig. 11 https://ui.adsabs.harvard.edu/abs/2019ApJ...879...36F/abstract
