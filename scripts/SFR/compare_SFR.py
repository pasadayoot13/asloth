def compare_SFR(folder):
    '''
    Compare total star formation rate density from A-SLOTH with observations

    parameters
    ----------

        folder : str
            output folder in which the file is saved and into which the plot will be saved
    '''
    import os.path
    import numpy as np
    import matplotlib.pyplot as plt
    from astropy.cosmology import Planck13 as cosmo#FlatLambdaCDM
    from astropy.cosmology import z_at_value
    import astropy.units as u
    from scipy.interpolate import interp1d
    from asloth_PlotConf import SetMyLayout


    folder_output = folder+"/plots/"
    SFR_file = folder+"/z_cSFR.dat"

    if (not os.path.isfile(SFR_file)):
        print("The file "+SFR_file+" does not exist.")
        return -99.

    data = np.genfromtxt(SFR_file,skip_header=2)
    data_B19 = np.genfromtxt('Data/SFR/Behroozi19.dat')
    data_M14 = np.genfromtxt('Data/SFR/Madau14.dat')

#A-SLOTH
    z = data[:,0]
    cSFRII = data[:,3]#PopII cosmic star formation rate density (Msun /yr /cMpc^3)
    cSFRIII = data[:,2]#PopIII cosmic star formation rate density (Msun /yr /cMpc^3)
    SFR_III_max = np.max(cSFRIII)

    t_asloth = [cosmo.age(z_now).value for z_now in z]
    t_B19 = [cosmo.age(z_now).value for z_now in data_B19[:,0]]
    t_M14 = [cosmo.age(z_now).value for z_now in data_M14[:,0]]

    if False: #Option for additional plots. Useful for debugging.
        plt.ylabel("$\mathrm{M}_\odot\,\mathrm{yr}^{-1}\,\mathrm{Mpc}^{-3}$")
        plt.yscale("log")
        plt.ylim((1e-5,0.1))
        plt.xlim((4,15))
        plt.scatter(z,cSFRII+cSFRIII,color='grey',label="A-SLOTH",marker="o")

        plt.axvline(10,color="k",linestyle=':')
        plt.axvline(np.min(z),color="k",linestyle=':')

        plt.plot(data_B19[:,0],data_B19[:,1],label="Behroozi+19",linestyle=':',linewidth=3)
        plt.plot(data_M14[:,0],data_M14[:,1],label="Madau&Dickinson14",linestyle=':',linewidth=3)

        plt.legend()
        plt.tight_layout()
        plt.savefig(folder_output+"compare_SFRD.pdf")
        plt.clf()


        plt.xlabel('Gyr')
        plt.ylabel('SFRD (M$_{sun}$/yr/Mpc$^{3}$)')

        plt.scatter(t_asloth,cSFRII+cSFRIII,color='grey',label="A-SLOTH")

        plt.axvline(cosmo.age(10.0).value,color="k",linestyle=':')
        plt.axvline(np.max(t_asloth),color="k",linestyle=':')

        plt.plot(t_B19,data_B19[:,1],label="Behroozi+19",linestyle=':',linewidth=3)
        plt.plot(t_M14,data_M14[:,1],label="Madau&Dickinson14",linestyle=':',linewidth=3)

        plt.yscale("log")
        plt.legend()
        plt.tight_layout()
        plt.savefig(folder_output+"compare_tSFRD.pdf")
        plt.clf()



## KS-like distance ##
## in time space because then the cumulative function is actually a mass density ##
    t_min = cosmo.age(10.0).value#highest redshift available from Behroozi+19
    t_max = np.max(t_asloth)#lowest redshift available from A-SLOTH
    Nbin = 32
    if(t_max-t_min > 13):
        Nbin = 256#Trees with outputs down to z=0
    if(t_min>t_max):
        print(t_min,">",t_max)
        print("No overlap between simulated redshift and observation")
        print("Do not use this constraint")
        return 0.0, -1.0
    x = np.linspace(t_min, t_max, num=Nbin, endpoint=True)

    f_asloth = interp1d(t_asloth, cSFRII+cSFRIII)
    y_asloth = f_asloth(x)

    f_B19 = interp1d(t_B19, data_B19[:,1])
    y_B19 = f_B19(x)

    f_M14 = interp1d(t_M14, data_M14[:,1])
    y_M14 = f_M14(x)

    if False: #Optional plot for debugging
        plt.plot(t_asloth,cSFRII+cSFRIII)
        plt.scatter(x,y_asloth)
        plt.plot(t_B19, data_B19[:,1])
        plt.scatter(x,y_B19)
        plt.plot(t_M14, data_M14[:,1])
        plt.scatter(x,y_M14)
        plt.yscale("log")
        plt.show()
        plt.clf()

    SetMyLayout()
# cumulative functions, i.e. integrate #
    c_asloth = np.zeros(Nbin)
    c_B19    = np.zeros(Nbin)
    c_M14    = np.zeros(Nbin)

    for i in range(1,Nbin):
        dt = (x[i]-x[i-1])*1e9#in Gyr
        c_asloth[i] = y_asloth[i] * dt + c_asloth[i-1]
        c_B19[i]    = y_B19[i]    * dt + c_B19[i-1]
        c_M14[i]    = y_M14[i]    * dt + c_M14[i-1]

# maximum distance #
    dist = np.zeros(Nbin)
    dist_obs = np.zeros(Nbin)#distance between observations

    c_asloth_log = np.log10(c_asloth+1e-10)
    c_B19_log = np.log10(c_B19+1e-10)
    c_M14_log = np.log10(c_M14+1e-10)

    for i in range(1,Nbin):#i=0 contains zeros of CDF
        sloth_now = c_asloth_log[i]
        if(sloth_now>c_M14_log[i]):
            dist[i] = sloth_now - c_M14_log[i]
        elif(sloth_now<c_B19_log[i]):
            dist[i] = c_B19_log[i] - sloth_now
        else:#between two observations
            dist[i] = 0
        dist_obs[i] = c_M14_log[i]-c_B19_log[i]
        #make distance relative
        dist[i] = dist[i] / dist_obs[i]

    SFR_max_dist = np.max(dist)
    print("Maximum Distance:",SFR_max_dist)
    i_max = np.where(dist == np.amax(dist))

#    vertical line at "worst" value
#    plt.axvline(x[i_max[0][0]],color="k",linestyle=':')

    #convert back to redshift
    #Depending on your version of astropy, it can help to add method="Bounded"
    z = [z_at_value(cosmo.age, tnow*u.Gyr,zmin=-0.1, zmax=49,ztol=1e-6,maxfun=1024) for tnow in x]

    plt.fill_between(z,c_M14,c_B19,color="g",alpha=0.5,linewidth=0)
    #first bin of CDF is 0 and produced artefact in log-space
    plt.plot(z[1:],c_M14[1:],label="Madau&Dickinson14")
    plt.plot(z[1:],c_B19[1:],label="Behroozi+19")
    plt.plot(z[1:],c_asloth[1:],label="A-SLOTH",c="k")
    #plt.xlim((min(z),max(z)))
    #plt.ylim((1e4,1e8))
    plt.yscale("log")
    plt.xlabel('redshift')
    plt.ylabel("cumulative SFRD ($\mathrm{M}_\odot\,\mathrm{Mpc}^{-3}$)")


    plt.legend()

    plt.tight_layout()
    plt.savefig(folder_output+"cumulative_SFRD.pdf",bbox_inches="tight")

    return SFR_max_dist, SFR_III_max
