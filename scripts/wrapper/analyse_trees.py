import numpy as np
import glob
import re
import os.path

import sys
sys.path.insert(0, 'scripts') # needed to import utility

from utilities.utility import asloth_fit
from plot_asloth import plot_comparison

#SET YOUR SPECIFIC PATH HERE
folder_name = ""#path where output is stored
#if empty, current directory will be used.
#should be same as path_output in loop_trees.py

para_list = ["IMF_min","IMF_max","slope",
        "ETAIII","ETAII","alpha_outflow","m_cha_out",
        "F_ESCIII","F_ESCII"]

def get_para_from_file(config_file):
    paras_now = []

    with open(config_file, 'r') as file:
        config_para = file.read().replace('\n', ' ')

    for para_now in para_list:
        m = re.search(para_now+" = (\S+)", config_para, re.IGNORECASE)
        paras_now.append(m.group(1))

    return paras_now


def get_p_from_folders(dir_prefix,ifEPS=True):
    '''
    Function that calculates the p-value (and additional values).

    Based on the directory prefix for the A-SLOTH output folder that should be analysed
    Function will you all folders that match this directory prefix.

    Parameters
    ----------
        dir_prefix : str
            directory prefix that should be used to search for data

    Returns
    -------
        fit_asloth : asloth_fit
            class object that contains fit results
    '''

    if(ifEPS):
        file_list = glob.glob(dir_prefix+"*")
        fit_asloth = plot_comparison(file_list)
        fit_asloth.get_p()
        return fit_asloth


    else:
    # 1. CTP trees: their folders start with H*
        file_list_all = glob.glob(dir_prefix+"H*")#MW trees
        #may contain folders of trees that are still running
        # MW_MDF.dat should exist
        print("File list before:",file_list_all)
        file_list = []
        for Hfolder in file_list_all:
            if(os.path.isfile(Hfolder+"/MW_MDF.dat")):
                file_list.append(Hfolder)
        print("File list after:",file_list)

        fit_asloth = plot_comparison(file_list)

    # 2. 8Mpc box: its folder starts with T8*
        file_list = glob.glob(dir_prefix+"T8*")#8Mpc box
        print("8Mpc box:",file_list)

        #check if file is available
        if(len(file_list) < 1):
            print("The output from 8Mpc does not exist.")
        else:
            SFR_file = file_list[0]+"/z_cSFR.dat"
            if (os.path.isfile(SFR_file)):
                #overwrite some values with cosmologicall representative 8Mpc box
                fit_asloth_8Mpc = plot_comparison(file_list)
                fit_asloth.SFR_integrated = fit_asloth_8Mpc.SFR_integrated
                fit_asloth.SFR_max_dist = fit_asloth_8Mpc.SFR_max_dist
                fit_asloth.tau_sigma = fit_asloth_8Mpc.tau_sigma
                fit_asloth.tau = fit_asloth_8Mpc.tau
                fit_asloth.SFR_III_max = fit_asloth_8Mpc.SFR_III_max


        fit_asloth.get_p()

        return fit_asloth




if __name__ == '__main__':
    from matplotlib import pyplot as plt
    file_output = open("Parameter_Exploration.dat",'w')
    #write header
    file_output.write("index ")
    for para_now in para_list:
        file_output.write(para_now+" ")
    fit_header = asloth_fit()#temporary class object to use its members to write header
    attrs = vars(fit_header)
    file_output.write(' '.join(str(item[0]) for item in attrs.items()))
    file_output.write(" \n")


    #find next index of output directories
    for i in range(0,1024):#checking up to 1024
        dir_prefix = folder_name+"output_para"+str(i)+"_"
        print("Is there a directory starting with "+dir_prefix+"?")
        file_list = glob.glob(dir_prefix+"*")
        print(len(file_list))
        if(len(file_list) < 2):
            break
        else:
            time_file = folder_name+"para_"+str(i)+"_time.txt"
            if os.path.isfile(time_file):
                print("The file "+time_file+" exists.")
                with open(time_file) as f:
                    runtime = f.readline()
                    runtime = runtime.rstrip('\n')
            else:
                print("The file "+time_file+" does not exist.")
                runtime=999


        fit_asloth = get_p_from_folders(dir_prefix,ifEPS=True)

        #obtain parameters from this run
        config_asloth = file_list[0]+"/USED_param.txt"
        print("Use config file: ",config_asloth)
        para_list_i = get_para_from_file(config_asloth)
        print(para_list_i)
        file_output.write(str(i)+" ")
        for para_now in para_list_i:
            file_output.write(para_now+" ")
        attrs = vars(fit_asloth)
        file_output.write(' '.join(str(item[1]) for item in attrs.items()))

        file_output.write(" \n")

        plt.close('all')

    file_output.close()
