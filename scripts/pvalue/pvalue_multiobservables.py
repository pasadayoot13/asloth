### Last updated on September 28th, 2022 by Li-Hsin ###
### Method description in Chen et al. 2022 arXiv:2209.11248. ### 
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats

import pvalue.obsdata as Obsdata
import pvalue.cluster_FFHtest as cluster_FFHtest

G_cgs = 6.67e-8 # cm^3 g^-1 s^-2
Msun  = 1.989e33 # g
pc    = 3.086e18 # cm
kpc   = 3.086e21 # cm
MV_SUN = 4.8 # solar V-band magnitude
MSURVIVE = 0.81

def computes_pvalue(UserFolder, UserLabels, folder_output, usedcol, if_write, if_fil, if_smclmc, \
  method, nc):
  """
  This computes a p-value by comparing the simulated Milky Way 
  satellite galaxies and observed ones with multiple observables. 
  The method is described in more details in Chen et al. 2022 arXiv:2209.11248.

  Note that this analysis is A-SLOTH specific, therefore we need to 
  randomly sample the solar position and prepare the data accordingly.

  The computation of the p-value utilizes unsupervised algorithms and 
  Fisher-Freeman-Halton test, which you need to have R and relevant 
  python packages (see System Requirements and Dependencies in the documentation). 

  Prerequisites:
    os, numpy, pandas, matplotlib,
    scipy, scikit-learn, seaborn, ryp2, and R. 
 
  Parameters:
      UserFolder (str): List of folder names to be plotted.
      UserLabels (str): List of labels. If none, UserFolder is used as labels.
      folder_output (str): where the plot is saved.
      usedcol (str): List of physical quantites of interest
      if_fil (bool): whether to apply the observational selection function from Koposov+09
      if_write (bool): whether to output the resulting p-values from 100 runs.
      if_smclmc (bool): whether to include LMC and SMC, which were not included in Chen et al. 2022. 
      method (str): which unsupervised clustering algorithm to use.
      nc (int): number of clusters. 

  Returns:
    A p-value that helps us reject the null hypothesis that the 
    simulated galaxies and observed galaxies are from the same 
    underlying distribution.
 
  """  

  ### From dark matter only merger trees, we do not know the exact position of the Sun.
  ### We therefore sample the solar position and run the analysis for 100 times.
  ### We take the geometric mean from the obtained 100 p-values. 
  ### Please read Chen+22 for more details. 

  nrun = 100
  for Folder, Label in zip(UserFolder, UserLabels):
    pv_list = np.zeros(nrun)
    nsat_list = np.zeros(nrun)
    for inr in range(nrun):
      # print (f'nc{nc} run {inr}')
      ### compute the p-value
      pv_list[inr], nsat_list[inr] = pv_randomsolarpos(_dir=Folder, Label=Label, usedcol=usedcol, \
        method=method, nc=nc, \
        if_fil=if_fil, if_smclmc=if_smclmc)
  
    if (if_write == True):
      file = f'{Folder}/{Label}_{method}_nc{nc}_100pv.txt'
      with open(file, 'w') as f:
        for inr in range(nrun):
          f.write(f'{pv_list[inr]} {nsat_list[inr]}\n')

  return (stats.gmean(pv_list), np.mean(nsat_list))

def pv_randomsolarpos(_dir, Label, usedcol, method, nc, if_fil, if_smclmc, if_norm=True):
  """
    Function to prepare data and compute the p-value

    Parameters:

      _dir (str): file path
      Label (str): label for this analysis
      usedcol (str): list of physical quantities
      method (str): unsupervised clustering algorithm

    Returns:
      p-value
      contingency table

  """
  feat_obs = prepare_observational_data(usedcol, if_fil=if_fil, if_smclmc=if_smclmc)

  ##### compute 1 p-value for 1 CTP tree #####
  datafile = _dir + '/Satellites/sats.dat'
  feat_asloth = prepare_asloth_data(datafile, if_fil=if_fil, if_smclmc=if_smclmc, usedcol=usedcol)
  nsat = len(feat_asloth['mstellar']) 
  
  ### Combine the observed data and simulated data to get the mean and standard deviation
  whole = pd.concat([feat_obs, feat_asloth], ignore_index=True)
  mean_list = []
  std_list = []
  for item in usedcol:
    mean = (np.mean(whole[item]))
    std = (np.std(whole[item]))
    if (if_norm == True):
      feat_obs[item]  = (feat_obs[item]-mean)/std
      feat_asloth[item] = (feat_asloth[item]-mean)/std    
    mean_list.append(mean)
    std_list.append(std)

  ### Compute the expected ratio between observed and simulated data
  epr = len(feat_obs['mhalo'])/(len(feat_asloth['mhalo'])+len(feat_obs))

  dfs = []
  dfs.append(feat_obs)
  dfs.append(feat_asloth)
  ### Actual computation of the p-value
  pv, ct = cluster_FFHtest.clustering_FisherFreemanHaltonExact(dfs=dfs, \
          nc=nc, exp_ratio=epr, method=method)

  return pv[0], nsat

def get_Mhalo_Errani_2018(Rhalf, sigma_v):
  """
    Function to estitmate mass enclosed in 1.8 Rhalf based on Errani et al. 2018

    Parameters: 
      Rhalf (float): half light radius in pc
      sigma_v (float):

    Returns:
      M (< 1.8 Rhalf) in Msun.

  """

  # mass enclosed in 1.8 Rhalf: M (< 1.8 Rh)
  # sigma_v: km/s
  # Rhalf: pc
  # Mhalo: Msun
  Mhalo = 3.5*1.8*(Rhalf*pc)*(sigma_v*1.e5)**2./G_cgs/Msun

  return Mhalo

def get_limit_Koposov2009(dist_helio):
  """
    Selection function from Koposov+09 2009ApJ...696.2179K

    Parameters:
      dist_helio (float): heliocentric distance in kpc

    Returns:
      Magnitude in solar units
  """
  return (1.1 - np.log10(dist_helio))/0.228
  
def MV_to_LV(MV):
  """
   Function to convert magnitude to luminosity in solar units
  """

  return 10**((Obsdata.MV_SUN-MV)/2.5)

def prepare_observational_data(usedcol, if_fil, if_smclmc):
  """
  This function prepares the observed MW satellites for the analysis.
 
  Parameters:
      usedcol (str): List of physical quantites of interest

      if_fil (bool): whether to apply the observational selection function from Koposov+09

      if_smclmc (bool): whether to include LMC and SMC, which were not included in Chen et al. 2022 arXiv:2209.11248.

  Returns:
      Observational data in pandas.Dataframe 

  """

  ##### Construct data, with additional derived quantities 
  feat_obs = pd.DataFrame(columns=usedcol)

  cols = Obsdata.obs_column
  # name, v band absolute magnitude, heliocentric distance (kpc), sigma_v (km/s), halo mass, 
  # dyn mass (r1/2), metallicity [Fe/H], standard deviation of [Fe/H], Rhalf (pc), 
  # 'FeH from McConnichie 2020', 'std FeH from McConnichie 2020'  
  obs_list =  pd.DataFrame(Obsdata._list, columns=cols)
  for ii in range(1, len(cols)):
    obs_list[cols[ii]] = obs_list[cols[ii]].astype('float64') # make sure we are working with float
  obs_list['mstellar'] = MV_to_LV(obs_list['MV'])
  obs_list['mstellar'] = np.log10(obs_list['mstellar'])
  obs_list['mhalo'] = 0.
  for ii in range(len(obs_list['mstellar'])):
    obs_list.loc[ii,'mhalo'] = \
      np.log10(get_Mhalo_Errani_2018(obs_list.loc[ii,'Rhalf'], obs_list.loc[ii,'sigmav']))+1 
    # in Chen+22, we assume that the virial mass is 10 times the mass enclosed in 1.8 Rhalf
    if (obs_list.loc[ii,'Mvir'] != 0):
      obs_list.loc[ii,'mhalo'] = obs_list.loc[ii,'Mvir']
      # if we actually found literature value for the virial mass, then use it
  obs_list['Rhalf'] = obs_list['Rhalf'].astype('float64')/1.e3 # convert from pc to kpc
  obs_list['MVKoposov2009'] = obs_list['mstellar']*(-2.5)+MV_SUN
  obs_list['limitKoposov2009'] = get_limit_Koposov2009(obs_list['distsun']) 
  # compute the lower limit of magnitude at certain heliocentric distance in order to be obsersved
  
  if if_fil == True:
    filobs = obs_list[ (obs_list['MVKoposov2009'] < obs_list['limitKoposov2009'] )].copy()
  else:
    filobs = obs_list

  ### concat the data to the output Dataframe
  feat_obs = pd.concat([feat_obs, filobs[usedcol]], ignore_index=True)

  ### Prepare the LMC and SMC data 
  if (if_smclmc == True):
    lsmc = pd.DataFrame(Obsdata.lsmc, columns=Obsdata.lsmc_col)
    cols = lsmc.columns
    for ii in range(1, len(cols)):
      lsmc[cols[ii]] = lsmc[cols[ii]].astype('float64')
    lsmc['mstellar'] = lsmc['log10LKS']
    for ii in range(len(lsmc['mstellar'])):
      lsmc.loc[ii,'mhalo'] = \
        np.log10(get_Mhalo_Errani_2018(lsmc.loc[ii,'Rhalf'], lsmc.loc[ii,'sigmav']))+1
      if (lsmc.loc[ii,'Mvir'] != 0):
        lsmc.loc[ii,'mhalo'] = lsmc.loc[ii,'Mvir']
    lsmc['MVKoposov2009'] = lsmc['mstellar']*(-2.5)+MV_SUN
    lsmc['limitKoposov2009'] = get_limit_Koposov2009(lsmc['distsun'])
    feat_obs = pd.concat([feat_obs, lsmc[usedcols]], ignore_index=True)

  ### Give label and index to the observed data
  feat_obs.loc[:,'ID'] = 'Obs.' #'Obs.'
  feat_obs.loc[:,'ind'] = 2
  
  return feat_obs

def prepare_asloth_data(datafile, usedcol, if_fil, if_smclmc):
  """
  This function prepares the simulated MW satellites from 1 CTP tree for the analysis.
 
  Parameters:
      datafile (str): filepath to the simulated satellites. Information stored in "sats.dat".

      usedcol (str): List of physical quantites of interest

      if_fil (bool): whether to apply the observational selection function from Koposov+09

      if_smclmc (bool): whether to include LMC and SMC, which were not included in Chen et al. 2022 arXiv:2209.11248.

  Returns:
      Observational data in pandas.Dataframe 

  """
  
  if os.path.isfile(datafile) and os.path.exists(datafile):
    asloth_list = pd.read_csv(datafile, delim_whitespace=True, header=None, skiprows=2)
    asloth_list.columns = ['x', 'y', 'z', 'mhalo', 'mhot', 'mcold', \
      'mstellar', 'id', 'meanFeH', 'stdFeH', 'distMW']
    asloth_list['MV'] = np.log10( asloth_list['mstellar'] )*(-2.5)+MV_SUN
    cosphi = np.random.uniform(-1, 1, len(asloth_list['MV']))
    asloth_list['distsun'] = np.sqrt(8.5**2 + (asloth_list['distMW']*1.e3)**2 - 2*8.5*cosphi) # Mpc to kpc
    asloth_list['MVKoposov2009'] = np.log10( asloth_list['mstellar'] )*(-2.5)+MV_SUN
    asloth_list['limitKoposov2009'] = get_limit_Koposov2009(asloth_list['distsun'])
    npre = len(asloth_list['MV'])
    asloth_list['mstellar'] = np.log10( asloth_list['mstellar'] )
    asloth_list['mhalo'] = np.log10( asloth_list['mhalo'] )
    if if_fil == True:
      ## Filtered data.
      temp_asloth = asloth_list[ (asloth_list['MVKoposov2009'] < asloth_list['limitKoposov2009'] )].copy()
      reduced_asloth = temp_asloth[ (temp_asloth['mstellar'] > 2) ].copy()
    else:
      reduced_asloth = asloth_list[ asloth_list['mstellar'] > 2 ].copy()
    if (if_smclmc == False):
      reduced_asloth = reduced_asloth[ reduced_asloth['mstellar'] <= 8]

    feat_asloth = reduced_asloth[usedcol].copy()
    feat_asloth['ID'] = 'A-SLOTH'
    return feat_asloth
  else: 
    print (f'{datafile} not found!')
    return pd.DataFrame(columns=['x', 'y', 'z', 'mhalo', 'mhot', 'mcold', \
      'mstellar', 'id', 'meanFeH', 'stdFeH', 'distMW'])

if __name__ == "__main__":
  ### exmample of how to use get p-value from one simulation run ### 
  _dir =  '/path/to/simulation/results/'
  pv_l, nsat_l = computes_pvalue(UserFolder=[_dir], UserLabels=['label'], folder_output=['./'], \
    usedcol=['mstellar', 'distsun', 'meanFeH', 'stdFeH', 'mhalo'], if_write=True, \
    if_fil=True, if_smclmc=False, method='Agglomerative', nc=5)
  # print (pv_l)
