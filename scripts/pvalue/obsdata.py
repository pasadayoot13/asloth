import numpy as np

MV_SUN = 4.83 # solar V-band magnitude
MKS_SUN = 3.28 # the Local Volume Catalog

obs_column =  [ 'name', 'MV', 'distsun', 'sigmav', 'meanFeH', \
'stdFeH', 'Rhalf', 'Mvir', 'FeHMc2020', 'errorFeHMc2020' ]
# name, v band absolute magnitude, heliocentric distance (kpc), sigma_v (km/s), halo mass, 
# dyn mass (r1/2), metallicity [Fe/H], standard deviation of [Fe/H], Rhalf (pc), 
# 'FeH from McConnichie 2020', 'std FeH from McConnichie 2020'
_list = np.array([
['SAGA_BooI',   '-6.02',  '66.9',  '4.6',   '-2.60', '0.42', '191', '7.0', '-2.55', '0.11'], # 
['SAGA_BooII',  '-2.94',  '42.0',  '10.5',  '-2.34', '0.65', '42' , '0'  , '-1.79', '0.05'], # 
['SAGA_CVnI',   '-8.73',  '211.0', '7.6',   '-1.91', '0.54', '211', '0'  , '-1.98', '0.01'], # 
['SAGA_CVnII',  '-5.17',  '160.0', '4.6',   '-2.21', '0.60', '162', '0'  , '-2.21', '0.05'],  # 
['SAGA_Car',    '-9.45',  '106.0', '6.6',   '-1.46', '0.55', '311', '0'  , '-1.72', '0.01'], # 
['SAGA_Com',    '-4.28',  '42.0',  '4.6',   '-2.72', '0.36', '69' , '8.3', '-2.60', '0.05'],  # 
['SAGA_Dra',    '-8.88',  '82.0',  '9.1',   '-1.97', '0.46', '231', '0'  , '-1.93', '0.01'], # 
['SAGA_For',    '-13.34', '139.0', '11.7',  '-1.10', '0.49', '792', '9.6', '-0.99', '0.01'], # 
['SAGA_GruI',   '-3.47',  '120.0', '2.9',   '-1.42', '0.40', '28' , '9.0', '-1.42', '0.55'], # upper/lower bound different  
['SAGA_Her',    '-5.83',  '132.0', '5.1',   '-2.43', '0.41', '216', '0'  , '-2.41', '0.04'],  # 
['SAGA_Hor',    '-3.76',  '87.0',  '4.9',   '-2.76', '0.17', '40' , '0'  , '-2.76', '0.10'], # 
['SAGA_LeoI',   '-11.78', '254.0', '9.2',   '-1.32', '0.34', '270', '9.0', '-1.43', '0.01'],  # 
['SAGA_LeoII',  '-9.74',  '233.0', '7.4',   '-1.56', '0.40', '171', '8.6', '-1.62', '0.01'], # 
['SAGA_LeoIV',  '-4.99',  '154.0', '3.3',   '-2.47', '0.50', '114', '0'  , '-2.54', '0.07'],  # 
# ['SAGA_LeoT',   '-8.00',  '409.0', '7.5',   '-2.08', '0.51', '118', '0', '-2.02', '0.20'  ], #  
['SAGA_PscII',  '-4.23',  '183.0', '5.4',   '-2.45', '0.48', '60'  , '0' , '-2.45', '0.07'],  # 
['SAGA_RetII',  '-3.99',  '31.6',  '3.3',   '-2.88', '0.52', '51'  , '0' , '-2.46', '0.09'], # upper/lower bound different  
['SAGA_Sgr',    '-13.50', '26.7',  '9.6',   '-0.54', '0.31', '2662', '0' , '-2.28', '0.03'],  # 
['SAGA_Scl',    '-10.82', '86.0',  '9.2',   '-1.86', '0.61', '279' , '9.0', '-1.68', '0.01'], # 
['SAGA_Seg1',   '-1.30',  '23.0',  '3.7',   '-2.52', '0.88', '24'  , '0'  , '-2.72', '0.40'],  # 
['SAGA_Seg2',   '-1.98',  '37.0',  '2.2',   '-2.24', '0.40', '40'  , '0'  , '-2.22', '0.13'], # 
['SAGA_Sex',    '-8.94',  '95.0',  '7.9',   '-2.12', '0.54', '456' , '8.48', '-1.93', '0.01'],  # 
['SAGA_TriII',  '-1.60',  '28.4',  '3.4',   '-2.43', '0.49', '16'  , '0'  , '-2.24', '0.08'], # 
['SAGA_TucII',  '-3.90',  '58.0',  '8.6',   '-2.94', '0.29', '121' , '0'  , '-2.23', '0.18'], # u/l diff 
['SAGA_TucIII', '-1.49',  '25.0',  '1.2',   '-2.42', '0.19', '37'  , '0'  , '-2.42', '0.07'], # u/l diff
['SAGA_UMa',    '-5.13',  '97.3',  '7.0',   '-2.04', '0.56', '295' , '0'  , '-2.18', '0.04'],  # 
['SAGA_UMaII',  '-4.43',  '34.7',  '5.6',   '-2.13', '0.68', '139' , '0'  , '-2.47', '0.06'], # 
['SAGA_UMi',    '-9.03',  '76.0',  '9.5',   '-2.04', '0.48', '405' , '0'  , '-2.13', '0.01'],  # 
['SAGA_WilI',   '-2.90',  '45.0',  '4.0',   '-1.40', '0.40', '33'  , '0'  , '-2.1', '-1']  #
], dtype=object)

lsmc_col = ['name', 'log10LKS', 'distsun', 'sigmav', 'meanFeH', 'stdFeH', 'Rhalf', 'Mvir']
lsmc = np.array([
  ['SAGA_SMC', '8.85',  '62.4',  '27.6',   '-0.95', '0.08', '1106', '0'],
  ['SAGA_LMC', '9.42',  '49.6',  '20.2',   '-0.37', '0.12', '2697', '0']
  ]
  )
