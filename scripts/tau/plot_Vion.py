from tau.determine_tau import calculate_tau

def plot_Vion(UserFolder, UserLabels, folder_output):
    """
    Function to plot ionisation histories and calculate tau
  
    Parameters
    ----------
        UserFolder: List[str]
            List of folder names to be plotted

        UserLabels: List[str]
            List of labels. If none, UserFolders are used as labels

        folder_output: str
            Folder in which plots will be saved

  
    Returns
    -------
        tau0: float
            optical depth to Thomson scattering

        tau_sigma: float
            tau uncertainty
  
    """

    import os.path
    import numpy as np
    import matplotlib.pyplot as plt
    from asloth_PlotConf import SetMyLayout

    SetMyLayout()

    fig = plt.figure(figsize=(4,3))
    ax = fig.add_subplot(111)
    for folder, label in zip(UserFolder, UserLabels):
        file_input = folder+"/vol_fracs.dat"
        if os.path.isfile(file_input):
            print("The file "+file_input+" exists.")
        else:
            print("The file "+file_input+" does not exist.")
            return -99., -99.

        data = np.genfromtxt(file_input, skip_header=1)

        # A-SLOTH
        z = data[:,0]
        f_rand = data[:,1]  # ion fraction by random sampling
        f_sum = data[:,3]/data[:,5]  # ion fraction by summing bubbles

        ax.plot(z, f_sum, label=label) #  adding bubbles
        # set max value of 1
        for i in range(len(f_rand)):
            f_rand[i] = min([1.,f_rand[i]])

    ##OPTICAL DEPTH##
    if(max(f_sum>0)):
        tau0, z, Q = calculate_tau(z, f_sum)
    else:
        tau0 = -99.

    ##LITERATURE##

    ##simulations##
    data_G15 = np.genfromtxt('Data/Vion/Graziani15.dat')
    data_dB17 = np.genfromtxt('Data/Vion/deB17.dat')


    ax.plot(data_G15[:,0], data_G15[:,1], label="Graziani+15", linestyle='dashed')
    ax.plot(data_dB17[:,0], data_dB17[:,1], label="de Bennassuti+17", linestyle='dashed')

    data_R15 = np.genfromtxt('Data/Vion/Robertson15.dat')
    ax.plot(data_R15[:,0], 1-data_R15[:,1], label="Robertson+15", linestyle='dashed', color='k')
    ##observation-based##
    add_obs_to_ion_fig(ax)

    ax.set_yscale("linear")
    ax.set_ylim((0, 1))
    ax.set_xlim((0, 20))

    ax.set_xlabel('redshift')
    ax.set_ylabel('ionized volume filling fraction')
    
    plt.legend(loc=9, ncol=2, bbox_to_anchor=(0.5, 1.47), frameon=False, fontsize=8)
    plt.savefig(folder_output+"z_Vion.pdf", bbox_inches="tight")
    plt.clf()

    # optical depth tau = 0.054 +/- 0.007, abstract of Planck18: https://arxiv.org/abs/1807.06209
    tau_sigma = np.abs(tau0-0.054)/0.007
    # difference in multiples of standard deviation

    return tau_sigma, tau0

def add_obs_to_ion_fig(ax):

    ## following compilation based on...
    # Masen+18, Fig. 12 https://ui.adsabs.harvard.edu/abs/2018ApJ...856....2M/abstract
    # Finkelstein+19, Fig. 11 https://ui.adsabs.harvard.edu/abs/2019ApJ...879...36F/abstract
    plot_opts = {"mfc":"white", "linewidth":0, "elinewidth":0.7, "capsize":2, "zorder":1, "ms":4, "markeredgewidth":0.7 }
    # Lya EW distribution from Masen+18, Fig. 12 https://ui.adsabs.harvard.edu/abs/2018ApJ...856....2M/abstract
    ax.errorbar(7, 1-0.592, xerr=0.5, yerr=0.13, marker="<", color="C2", **plot_opts, label=r"Lyman-$\alpha$ EW")

    # Lya and Lb forest dark fraction from McGreer+15 https://ui.adsabs.harvard.edu/abs/2015MNRAS.447..499M/abstract
    ax.errorbar([5.6,5.9,6.1], [1-0.09,1-0.11,1-0.39], yerr=[0.05,0.05,0.05], lolims=True, **plot_opts, marker="p", color="grey", label=r"Ly-$\alpha$ forest")

    # Planck Collaboration 2016 redshift range of instantaneous reionization: https://ui.adsabs.harvard.edu/abs/2016A%26A...596A.108P/abstract
    ax.errorbar(8.3, 0.5, xerr=1.9, marker="d", color="C6", label="Planck inst. reionization", **plot_opts)

    # fraction of LBGs emitting Lya, Mesinger et al. 2015: https://ui.adsabs.harvard.edu/abs/2015MNRAS.446..566M/abstract
    ax.errorbar(6.9, 1-0.4, yerr=0.04, uplims=True, color="C1", marker="s", **plot_opts, label=r"Ly-$\alpha$ emission")

    # QSO damping wings Greig & Mesinger 2017b, Banados et al. 2018
    # https://ui.adsabs.harvard.edu/abs/2017MNRAS.465.4838G/abstract
    # https://ui.adsabs.harvard.edu/abs/2018Natur.553..473B/abstract
    ax.errorbar([7.1,7.5], [1-0.4,1-0.55], yerr=[0.2,0.2], marker="o", color="C5" , label="QSO damping wings", **plot_opts)

    # emission from Lya clustering (Ouchi et al. 2017) https://ui.adsabs.harvard.edu/abs/2018PASJ...70S..13O/abstract
    ax.errorbar(6.65, 1-0.85, yerr=0.15, color="C4", marker=">", **plot_opts, label=r"Ly-$\alpha$ clustering")

    # Lya luminosity function evolution (Konno et al. 2014, 2017; Zheng et al. 2017)
    # https://ui.adsabs.harvard.edu/abs/2018PASJ...70S..16K/abstract
    # https://ui.adsabs.harvard.edu/abs/2017ApJ...842L..22Z/abstract
    ax.errorbar([6.56,6.91,7.30], [1-0.70,1-0.5,1-0.451], yerr=[0.25,0.25,0.25], marker="x", color="C3", **plot_opts, label=r"Ly-$\alpha$ luminosity function")
    return

def get_tau_sigma(folder):
    """
    Function to calculate tau without plotting ionisation history

    Parameters
    ----------
        folder: str
            Folder in which plots will be saved

  
    Returns
    -------
        tau0: float
            optical depth to Thomson scattering

        tau_sigma: float
            tau uncertainty

    """

    import os.path
    import numpy as np


    file_input = folder+"/vol_fracs.dat"
    if os.path.isfile(file_input):
        print("The file "+file_input+" exists.")
    else:
        print("The file "+file_input+" does not exist.")
        return -99., -99.

    data = np.genfromtxt(file_input, skip_header=1)

    #A-SLOTH
    z = data[:,0]
    f_rand = data[:,1]  # ion fraction by random sampling
    f_sum = data[:,3]/data[:,5]  # ion fraction by summing bubbles

    # set max value of 1
    f_rand[f_rand > 1.0] = 1.0

    ##OPTICAL DEPTH##
    if(max(f_rand>0)):
        print("Calculation of tau not yet checked!!!")
        tau0, z, Q = calculate_tau(z, f_rand)
    else:
        tau0 = -99.


    ##simulations##
    # optical depth tau = 0.054 +/- 0.007, abstract of Planck18: https://arxiv.org/abs/1807.06209
    tau_sigma = np.abs(tau0-0.054)/0.007
    # difference in multiples of standard deviation

    return tau_sigma, tau0
