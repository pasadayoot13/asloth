import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axisartist.axislines import Subplot

import sys
sys.path.insert(0, 'scripts') # needed to import SetMyLayout
from asloth_PlotConf import SetMyLayout

MSUN_cgs=1.989e33
J2ERG=1.0e7
k_Boltzmann=1.3806503e-23
k_Boltzmann_erg=k_Boltzmann*J2ERG
YHE=0.2477
MEAN_MOL=1.0/(1.0-0.75*YHE)
M_Atomic_g=M_Atomic=1.66053873e-24
G_cgs=6.674e-8
MPC_cgs=3.0857e24
HUBBLE=0.6774
OMEGA_m=0.3089
HUBBLE_cgs=HUBBLE*100.0*1.0e5/MPC_cgs
RHO_CRIT_cgs=(3.0*HUBBLE_cgs*HUBBLE_cgs)/(8.0*3.141593*G_cgs)
RHO_m_cgs=OMEGA_m*RHO_CRIT_cgs
T_CRIT=2200.

def crit_mass(z, lw_mode, VBC):
  """
  Function to obtain ciritical mass of a halo at certain redshift, given by different models 

  We use lw_mode=5 (Schauer et al. 2021) as the fiducial model. 
  Use can also choose lw_mode=4, which is a combined model from O'Shea et al. 2008, Stacy et al. 2011, and Hummel et al. 2012. lw_mode=7 corresponds to a model from Fialkov et al. 2013. 
 
  Parameters:
    z (float): redshift
    lw_mode (int): which M_crit model
    VBC (float): initial streaming velocity, in units of sigma_v

  Returns:
    Critical mass of a halo to have star formation
 
  """  
  if(lw_mode == 5 or lw_mode == 6):
    crit_mass = get_Mcrit_Schauer(z, lw_mode, VBC)
  elif (lw_mode == 7):
        crit_mass = get_Mcrit_Fialkov(z, VBC)
  else:
    crit_mass = get_Mcrit_Stacy(z, VBC)
   
  if (lw_mode >= 5):
    # return crit_mass
    return np.minimum(crit_mass,ion_mass(z))
    
  if (lw_mode == 4):
    mob = get_Mcrit_OsheaNorman(z)
    mcrit = np.minimum(crit_mass,ion_mass(z))
    return mcrit

def get_Mcrit_Stacy(z, VBC):
  # Modified to also check Jeans Mass calculated with modified speed of sound
  # See Stacy et al. 2011

  # for supersonic streaming baryon motion:
  v_stream_current = VBC*6.0e5/201.0*(1+z) # current streaming velocity in cm/s
  # (VBC is relative to 1 sigma .i.e. 6 km/s @ z=200)
  T_b = 0.017*(1+z)*(1+z) # Baryonic Temperature: Schneider ch. 10.3 pg 530
  # speed of sound squared
  c_s_2 = 5./3.*k_Boltzmann_erg*T_b/(MEAN_MOL*M_Atomic_g)
  # modified speend of sound squared:
  c_mod_2 = v_stream_current**2+c_s_2
  # modified Jeans mass
  crit_mass_stacy = np.pi**(2.5)/6.0*(RHO_m_cgs*(1+z)**3)**(-0.5)/MSUN_cgs*(c_mod_2/G_cgs)**(1.5)

  # Selecting maximum of cooling mass and baryon streaming Jeans mass:
  return np.maximum(get_Mcrit_Hummel(z),crit_mass_stacy)

def get_Mcrit_Schauer(z, lw_mode, VBC):
  # based on Schauer+20,arXiv:2008.05663
  # Criticl mass for halo collapse with LW feedback and baryonic streaming

  J21 = 10.0**(2.0-z/5.0) # according to fit in Greif&Bromm 2006

  if (lw_mode == 5): # Schauer+21,arXiv:2008.05663
    logM0 = 6.0174 * (1.0 + 0.166 * np.sqrt(J21)) # Eq.9
    get_Mcrit_Schauer = 10.0**(logM0 + 0.4159 * VBC)
  elif (lw_mode == 6): # Schauer+21,arXiv:2008.05663
    logM0 = 5.562 + np.log10(1.0 + 0.279 * np.sqrt(J21)) # Eq.11
    s = 0.614 * (1-0.560*np.sqrt(J21))
    get_Mcrit_Schauer = 10.0**(logM0 + s * VBC)
  else:
    print ("WARNING: lw_mode not compatible with Mcrit")
    return 0

  return get_Mcrit_Schauer

def get_Mcrit_Hummel(z):
  # function determines the critical mass for H2 cooling, from Hummel et al 2012
  return 1.e6*(T_CRIT/1.e3)**(1.5) * (0.1*(z+1))**(-1.5)

#def atom_mass(z):
#  # function determines the critical mass for atomic cooling, following Glover2013 in "the first galaxies"
#  return 4.5e7*((0.1*(z+1))**(-1.5)) # in solar masses

def ion_mass(z): 
  return 1.e6*(10.0)**(1.5)*(0.1*(z+1))**(-1.5) # 10^4 K virial temp

def T_vir(M,z): 
  return 1000.0*(M*1.0e-6)**(2.0/3.0)*(1+z)*0.1

def get_Mcrit_Fialkov(z, VBC):
  # based on Fialkov+12 and Fialkov+13, MNRAS 424, 1335–1345 and MNRAS 432, 2909–2916
                                                                                        
  v_stream_current = VBC*6.0/201.0*(1+z) # current streaming velocity in km/s
  V0 = 3.714 # km s^-1, circular velocity
  alpha = 4.015
  J21 = 10.0**(2.0-z/5.0) # according to fit in Greif&Bromm 2006                         
  Vcool = np.sqrt( V0**2 + (alpha*v_stream_current)**2 ) # km s^-1  eq. 2 in MNRAS 424, 1335–1345
  M0 = ( (Vcool/146.6)**3. ) * ( OMEGA_m**(-0.5) ) * ( (1+z)**(-1.5) ) * 1.e12 
  # h^-1 Msun, derived from Brayn&Norma 1998, 1998ApJ...495...80B 
  # assuming r_vir happens at r_200
                                                                                        
  return M0*( 1 + 6.96 * ((4*np.pi*J21)**0.47) ) # Msun, eq. 1 in MNRAS 432, 2909–2916                         

def get_Mcrit_OsheaNorman(z):
  tmp = 4.0*np.pi*10.0**(2.0-z/5.0)
  return 4*(1.25e5+8.7e5*(tmp)**0.47)

def plot_vbcdependence(VBC, i1, i2, ax):
  cs = ['g', 'r', 'b', 'k']
  nbin = 64
  za = np.linspace(30, 5, num=nbin, endpoint=True) 
  ohs = np.zeros(nbin)
  s2110 = np.zeros(nbin)
  f13 = np.zeros(nbin)
  ACM = np.zeros(nbin)

  for ii in range(len(za)):
    ohs[ii] = crit_mass(z=za[ii], lw_mode=4,   VBC=VBC)
    s2110[ii] = crit_mass(z=za[ii], lw_mode=5, VBC=VBC)
    f13[ii] = crit_mass(z=za[ii], lw_mode=7,   VBC=VBC)
    ACM[ii] = ion_mass(za[ii])

  if(False):#also plot data from Valiante+16
    cs.append("m")
    data_V16 = np.genfromtxt('Data/z_Mcrit_Valiante16.dat')
    ax[i1,i2].plot(data_V16[:,0],data_V16[:,1],   label='Valiante+16', alpha=0.5, c=cs[4])


  ax[i1,i2].plot(za, ohs,   label='OHS', alpha=0.5, c=cs[0])
  ax[i1,i2].plot(za, s2110, label='Fiducial: S21', alpha=0.5, c=cs[1],linewidth=4)
  ax[i1,i2].plot(za, ACM, label='ACH',   alpha=0.5, c=cs[3])
  ax[i1,i2].plot(za, f13,   label='F13', alpha=0.5, c=cs[2])
  ax[i1,i2].text(x=20, y=6.e7, s='$v_\mathrm{BC} = $'+str(VBC))
  ax[i1,i2].set_ylim(5.e5, 1.e8)
  ax[i1,i2].set_yscale('log')
  if (i1 == 0 and i2 == 0):
    ax[i1,i2].legend(bbox_to_anchor=(0, 1, 1, 0), loc="lower left", ncol=2, frameon=False)
 
 
def plot_ohs(VBC, i1, i2):
  nbins = 41
  za = np.linspace(25, 5, num=nbins, endpoint=True)
  # o = np.zeros(nbins)
  s = np.zeros(nbins)
  h = np.zeros(nbins)
  i = np.zeros(nbins)
  c = np.zeros(nbins)
  for ii in range(len(za)):
    s[ii] = get_Mcrit_Stacy(z=za[ii], VBC=VBC)
    h[ii] = get_Mcrit_Hummel(z=za[ii])
    i[ii] = ion_mass(za[ii])
  c[:] = np.maximum(s[:], h[:])
  c[:] = np.minimum(c[:], i[:])
  ss = 3
  ax[i1].scatter(za, s, alpha=0.5, label='Stacy', s=ss)
  ax[i1].scatter(za, h, alpha=0.5, label='Hummel', s=ss)
  ax[i1].scatter(za, i, alpha=0.5, label='Ion mass', s=ss)
  ax[i1].scatter(za, c, alpha=0.5, label='Final', color='k', s=ss)
  ax[i1].set_yscale('log')
  if (i1 == 1):
    ax[i1].legend(loc='best', fontsize=8)
  if (i1 == 0):
    ax[i1].set_ylabel('$M_\mathrm{crit}/M_{\odot}$')
  ax[i1].set_xlabel('z')
  ax[i1].set_title('$v_\mathrm{BC} = $'+str(VBC))
  ax[i1].set_ylim(5.e5, 1.e9)

def plot_mcrit():
  """
  Function to plot ciritical mass of a halo at certain redshift, given by different models, and at different initial streaming velocities. 

  Returns:
    Figure that shows M_crit v.s. z at different initial streaming velocities and from different models
 
  """

  fa = SetMyLayout(ratio = 1.0)
  ax = fa.subplots(nrows=2, ncols=2, sharex=True, sharey=False) 
  fa.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0., hspace=0.)
  for ii in range(2):
    for jj in range(2):
      ax[ii,jj].tick_params(axis='both', which='both', direction='in', color='k')
      ax[ii,jj].set_xlim([5,30])
  plot_vbcdependence(VBC=0,   i1=0, i2=0, ax=ax)
  plot_vbcdependence(VBC=0.8, i1=0, i2=1, ax=ax)
  plot_vbcdependence(VBC=2,   i1=1, i2=0, ax=ax)
  plot_vbcdependence(VBC=3,   i1=1, i2=1, ax=ax)
  for ii in range(2):
    ax[1, ii].set_xlabel('z')
  for jj in range(2):
    ax[jj, 0].set_ylabel('$M_\mathrm{crit}/M_{\odot}$')
    ax[jj, 1].set_yticklabels('')
  plt.savefig('./mcrit_vBC.pdf', format='pdf', dpi=300, bbox_inches='tight')
  plt.close(fa)  

if __name__ == '__main__':
  plot_mcrit()

