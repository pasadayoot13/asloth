import numpy as np
from numpy import f2py
from glob import glob

class slothfile:
    def __init__(self, name):
        self.name = name
        self.modules = []
        self.used_by = []
        self.uses = []
        self.level = 1
        main_name = "src/trees.F90"
        if name == main_name:
            self.level = 0
    
    def get_dep_command(self, files):
        tmp = get_o_name(self.name)+":"
        for k in self.uses:
            tmp = tmp + " " + get_o_name(files[k].name)
        split = self.name.split("/")
        if len(split) > 2:
            tmp = tmp + " |" + split[1]
        return tmp+"\n"
        
def get_o_name(fname):
    # convert the source directory into the name of the .o file
    base = fname.split("src/")[1]
    base = base.split(".F90")[0]
    return "$(BUILD_DIR)/"+base+".o"

# list of files to be analysed
f90files = glob("src/*.F90")+glob("src/*/*.F90")

# parse fortran code
all_blocks = []
f2py.crackfortran.quiet=True
print("Starting to parse fortran files")
for f in f90files:
    new_blocks  = f2py.crackfortran.crackfortran(f)
    for nb in new_blocks:
        if "from" not in nb.keys():
            nb["from"] = f
    all_blocks = all_blocks + new_blocks
print("done parsing, now analysing")

# making list of all blocks that are modules
modules = []
for block in all_blocks:
    if block["block"] == "module":
        modules.append(block)

# creating custom file dictionary
files = {}
for bl in all_blocks:
        if bl["from"] not in files.keys():
            files[bl["from"]] = slothfile(bl["from"])

# assign modules to files
for m in modules:
    files[m["from"]].modules.append(m["name"])

# analyse use associations
for b in all_blocks:
    # this part captures "use" commands in module declaration sections
    if "use" in b.keys():
        for u in b["use"]:
            for k in files.keys():
                if (u in files[k].modules and b["from"] not in files[k].used_by
                    and k != b["from"]):
                    files[k].used_by.append(b["from"])
                    files[b["from"]].uses.append(k)
        
    # This part captures the associations in subroutines and functions
    for bo in b["body"]:
        if "use" in bo.keys():
            for u in bo["use"]:
                for k in files.keys():
                    from_name = bo["from"].split(":")[0]
                    if u in files[k].modules:
                        if (from_name not in files[k].used_by
                            and from_name != k):
                            files[k].used_by.append(from_name)
                            files[from_name].uses.append(k)

    

print("writing outputs")
#write dependencies into file
f = open("dependencies.mk", "w")
for k in files.keys():
    line = files[k].get_dep_command(files)
    f.write(line)
f.close()
print("complete")
