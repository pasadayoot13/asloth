#-*-coding:utf-8 -*- 

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import sys
sys.path.insert(0, 'scripts') # needed to import SetMyLayout
from asloth_PlotConf import SetMyLayout

def makeplots(df, stype, mid1, mid2, fname):
  fa, axs = plt.subplots(3, 2)
  titlesize=10
  axs[0, 0].scatter(df['M'], df['P']/df['M']*1000)
  axs[0, 0].set_ylabel('N (/1000$M_\odot$)', fontsize=titlesize)
  axs[0, 1].plot(df['M'], df['Q'])
  axs[0, 1].set_ylabel('Q (1/s)', fontsize=titlesize)
  axs[1, 0].scatter(df['M'], df['tl'])
  axs[1, 0].set_ylabel('lifetime (yr)', fontsize=titlesize)
  axs[1, 1].scatter(df['M'], df['IoM'])
  axs[1, 1].set_ylabel('$\\dot{M} (Msun/s)$', fontsize=titlesize)
  axs[2, 0].scatter(df['M'], df['SNE'])
  axs[2, 0].set_ylabel('SN energy (erg)', fontsize=titlesize)
  axs[2, 1].scatter(df['M'], df['P']/df['M']*1000, color='none')

  ticksize=5
  mmin = int(np.min(df['M']))
  mmax = int(np.max(df['M']))
  for ii in range(3):
    for jj in range(2):
      axs[ii, jj].set_yscale('log')
      axs[ii, jj].set_xscale('log')
      axs[ii, jj].set_xticks( [mmin, mid1, mid2, mmax] )
      axs[ii, jj].set_xticks( [mmin, mid1, mid2, mmax] )
      axs[ii, jj].tick_params(axis='both', which='both', direction='in', color='k',labelsize=ticksize)
      axs[ii, jj].set_xticklabels(['' for ii in range(4)])
  axs[2, 1].tick_params(axis='both', which='both', direction='in', color='none',labelsize=ticksize)
  axs[2, 1].set_yticklabels(['' for ii in range(4)])

  axs[1, 1].set_ylim(1.e-15, 1.e-9)
  axs[2, 0].set_xlabel('Mass')
  axs[2, 1].set_xlabel('Mass')
  axs[2, 0].set_xticklabels( [str(mmin), str(mid1), str(mid2), str(mmax)] )
  axs[2, 1].set_xticklabels( [str(mmin), str(mid1), str(mid2), str(mmax)] )

  # fa.tight_layout()
  fa.suptitle('Pop '+stype)
  fa.savefig('StellarProp'+stype+'_'+fname+'.png', format='png', dpi=360, bbox_inches='tight')
  plt.close(fa)

def makeplots_comp(axs, df, color, label, marker):

  ssize = 6
  if(label == "Pop III"):
    ssize += 3 #  so that it can be better seen behind
  ap = 1.0

#define valid mass ranges base don Schaerer02
  if(label == "Pop II"):
    mask = [x and y for x,y in zip(df['M'] < 150, df['M'] > 7)]
  else:
    mask = [x and y for x,y in zip(df['M'] < 500, df['M'] > 9)]
  axs[0, 1].plot(df['M'][mask], df['Q'][mask], color=color, linewidth=ssize-5, alpha=ap, label=label)
  axs[0, 1].set_ylabel('Q (1/s)')

  if(label == "Pop II"):
    mask = (df['M'] < 200)
  else:
    mask = [x and y for x,y in zip(df['M'] < 500, df['M'] > 0.7)]
  axs[1, 1].plot(df['M'][mask], df['tl'][mask], color=color, linewidth=ssize-5, alpha=ap, label=label)
  axs[1, 1].set_ylabel('lifetime (yr)')
  axs[0, 0].scatter(df['M'], df['SNE'], color=color, marker=marker, s=ssize, alpha=ap, label=label)
  axs[0, 0].set_ylabel('SN energy (erg)')
  axs[0, 0].annotate("CCSNe",[13,4e50],color="lightgrey")
  axs[0, 0].annotate("PISNe",[130,1.4e52],color="lightgrey")


  axs[1, 0].set_ylabel('$M_{\\rm eject}$ (M$_\odot$)')
  if(label == "Pop II"):
    c1 = "cyan"
    c2 = "green"
  else:
    c1 = "orange"
    c2 = "magenta"
  axs[1, 0].scatter(df['M'], df['C'], color=c1, marker=marker, s=ssize, alpha=ap, label='C, '+label)
  axs[1, 0].scatter(df['M'], df['Fe'], color=c2, s=ssize, marker=marker, alpha=ap, label='Fe, '+label)

#xranges
  for ax_left in axs[:,0]:
    ax_left.set_xlim(1, 400)
  for ax_right in axs[:,1]:
    ax_right.set_xlim(0.5, 1e3)

  mmin = int(np.min(df['M']))
  mmax = int(np.max(df['M']))
  for ii in range(2):
    for jj in range(2):
      axs[ii, jj].set_yscale('log')
      axs[ii, jj].set_xscale('log')
  plt.setp(axs[0, 1].get_xticklabels(), visible=False)
  plt.setp(axs[0, 0].get_xticklabels(), visible=False)


  axs[1, 1].set_ylim(1.0e5, 3.e12)
  axs[0, 0].set_ylim(1e50, 1.e53)
  axs[0, 1].set_ylim(1.0e43, 1e55)
  axs[1, 0].set_ylim(1e-3, 1e2)

  axs[1, 0].set_xlabel('Stellar Mass (M$_\odot$)')
  axs[1, 1].set_xlabel('Stellar Mass (M$_\odot$)')

def plot_imf(df, stype, fname):
  fa = plt.figure()
  plt.scatter(df['M'], df['P']/df['M']*1000)
  plt.savefig('Pop'+stype+'_IMF'+fname+'.png', format='png', dpi=360, bbox_inches='tight')
  plt.close(fa)

def plot_stellarproperties():
  """
  Function to reproduce the stellar properties plot  
  
  Returns:
    Stellar masses v.s. stellar properties, e.g. stellar lifetime, carbon yields, iron yields, supernova energie.

  """  
  _dir = './stellarprop/'
  datafile = _dir+'PopIII_val.dat'
  dfiii = pd.read_csv(datafile, delim_whitespace=True, skiprows=1, header=None)
  dfiii.columns = ['M', 'P', 'Q', 'tl', 'IoM', 'SNE', 'C', 'Fe', 'Total']

  datafile = _dir+'PopII_val.dat'
  dfii = pd.read_csv(datafile, delim_whitespace=True, skiprows=1, header=None)
  dfii.columns = ['M', 'P', 'Q', 'tl', 'IoM', 'SNE', 'C', 'Fe', 'Total']

  marigo2001 = pd.read_csv('../Data/Marigo2001.txt', delim_whitespace=True, skiprows=1, header=None)
  marigo2001.columns = ['M', 'tau_H0', 'tau_H1', 'tau_He0', 'tau_He1']
  marigo2001['tl'] = marigo2001['tau_H0']*marigo2001['tau_H1'] + marigo2001['tau_He0']*marigo2001['tau_He1']

  esktroem2008 = pd.read_csv('../Data/Esktroem2008.txt', delim_whitespace=True, skiprows=1, header=None)
  esktroem2008.columns = ['M', 'tl']
  esktroem2008['tl'] = esktroem2008['tl']*1.e6

  fig = SetMyLayout(col=2, ratio = 0.4)

  axs = fig.subplots(2, 2)

  makeplots_comp(axs, dfiii, 'r', 'Pop III','o')
  makeplots_comp(axs, dfii, 'b', 'Pop II','x')
  axs[1,1].scatter(marigo2001['M'], marigo2001['tl'], label='Marigo+01', s=8, c='grey', alpha=0.75, zorder=128)
  axs[1,1].scatter(esktroem2008['M'], esktroem2008['tl'], label='Ekstroem+08', s=8, c='k', alpha=0.75, zorder=128)
  axs[1,1].legend() #  lifetime literature
  axs[1,0].legend(loc = "upper left") #  yields
  axs[0,0].legend()
  axs[0,1].legend()

  fig.subplots_adjust(wspace=0.3, hspace=0.15)

  fig.savefig('./StellarProp.pdf', format='pdf', bbox_inches='tight')
  plt.close(fig)

if __name__ == '__main__':
  plot_stellarproperties()
