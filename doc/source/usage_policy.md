# Usage Policy

You can use A-SLOTH freely for scientific research and other applications in accordance with the license. If you publish results for which you have used A-SLOTH, you must cite the following two papers:
- Hartwig et al. 2022, [published in ApJ](https://ui.adsabs.harvard.edu/abs/2022ApJ...936...45H/abstract)
- Magg et al. 2022, [published in JOSS](https://joss.theoj.org/papers/10.21105/joss.04417)

In addition, you can cite the code on [zenodo](https://zenodo.org/record/6683682#.YrkA3HhByEA).

You do not have to invite the A-SLOTH developers as co-authors on your publications. If you wish to contribute to A-SLOTH, please get in contact with us and/or create a pull-request.
