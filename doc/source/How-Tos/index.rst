.. ASLOTH documentation master file, created by
   sphinx-quickstart on Fri Sep 24 13:46:07 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

How Tos
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::
   debug.md

.. toctree::
   metals.md

.. toctree::
   NBODY.md

.. toctree::
   CheckChanges.md

