(debug_target)=
# Debugging
If A-SLOTH is not behaving as expected, this section might help. If the code is not compiling, please check the [code requirements](Requirements_target).
A-SLOTH required several GB of RAM. Please check your available RAM before and during the run with `top` or `free -m`.
If the code is running but crashes (e.g. segmentation fault) we recommend external debugging software such as valgrind.
If the code is running, but the results seem strange to you, you can try the options below.
If you think you found a bug, please report it via the GitLab issue tracker or by e-mail to hartwigATphys.s.uMINUStokyo.ac.jp.

## Compiler Options
In the file `src/asloth.h` you can activate the compile time options `DEBUG`, `INFO`, and `MEM_REPORT`, which will provide additional outputs, especially if something is strange.

## Python Scripts
The following python functions can be useful to identify problems with the code:
- `scripts/plot_asloth.py`
- `scripts/plot_tgas.py`

## Additional Output
You can print additional information to the terminal or a file to check if variables during the run have the values that you expect. We provide the subroutine `print_all_props()` to print additional information of nodes (see file `src/defined_types.F90`).

# Common Issues
## Git clone does not work
If you get the error `SSL_ERROR_SYSCALL in connection to gitlab.com:443` or similar, you can also manually download the source code from the [repository website](https://gitlab.com/thartwig/asloth).


## The code does not compile
This can occur when you modified the 'asloth.h' or changed how you compile (e.g. the compiler and/or optimization flags) in the middle of compiling.
It may also be helpful recompile the code entirely with
```
make clean
make
```
If the reason for being unable to compile is that the compiler is unable to find a module, please re-generate the dependency list via
```
python scripts/utilities/make_dependency_list.py
```
## The generation of the dependency list fails
If the generation of the dependency list gets stuck and encounters a stack overflow after a while, most likely a circular dependency has been introduced. Be aware that if `module A` uses `module B`, neither `module B` nor any module that uses it can use `module A` 

## A-SLOTH does not accept my parameter.nml file
This file contains a set of fortran namelists. All namelists need to be present, even if they are empty. The names of all given parameters need to be identical to the names of the parameters in the specified namelist in the code. They also must have the correct type, and - if its an array - length. Missing quotation marks for a string or superflous quotation marks for a non-string variable are a likely reason.

## The code crashes with a strange memory error
Make sure you compile with the options `-fbounds-check` (gfortran) or `-CB` (ifort). The most common reason for such memory errors is stepping out of bounds of an array. If the error only occurs with ifort but not gfortran, make sure to use the `-heap-arrays` flag for compiling. Compiling with `-g` (if you do not do so already) will give you slightly improved debugging information. If you recieve a segmentation fault while writing a very long line to a text file, try increasing the `recl` parameter when opening the offending file.

## Compilation with ifort crashes
If your compilation crashes with
```
ifort -c  src/stellar/IMF.F90 -O3 -fpp -xHost -ipo -heap-arrays -g -r8 -D IFORT -shared-intel -module mods -o build/stellar/IMF.o
src/stellar/IMF.F90(53): warning #6178: The return value of this FUNCTION has not been defined.   [RES]
  function P_func_dummy(this, m1, m2) result(res)
---------------------------------------------^
```
you can try to use a newer version of the ifort compiler or the gfortran compiler instead.


## Compilation crashes with complaint about finalizers
The error message could look like this:
```
src/defined_types.F90:80.25:

     type(Stars_HighMass), pointer :: HighStars => null()
                         1
Error: Finalization at (1) is not yet implemented
make: *** [build/defined_types.o] Error 1
```
Finalizers are only recognized since gcc 4.9. So you probably need a newer version of your compiler.



## A-SLOTH crashes with end of file error
```
At line 46 of file src/cosmic_time.F90
Fortran runtime error: End of file
```
Make sure that the value of `nlev` is correct in the parameter file.


## The standard python plotting script does not work
If you try to plot output and it crashes with this error
```
asloth$ python scripts/plot_asloth.py
Traceback (most recent call last):
  File "scripts/plot_asloth.py", line 156, in <module>
    MyFit = plot_comparison(UserFolder, UserLabels=UserLabels)
  File "scripts/plot_asloth.py", line 51, in plot_comparison
    from utilities.utility import asloth_fit
ImportError: No module named utilities.utility
```
or similar, you can try to run it explicitely with python3 (you should always run the plotting scripts with python3).
