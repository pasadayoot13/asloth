# Help
If you encounter any problems with A-SLOTH, there are several options to help you:
- Check the section on [debugging](debug_target).
- Create an issue on GitLab.
- Contact the developers: GloverATuni-heidelberg.de
