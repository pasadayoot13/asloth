.. ASLOTH documentation master file, created by
   sphinx-quickstart on Fri Sep 24 13:46:07 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

A-SLOTH Documentation
=====================

.. image:: figures/ASLOTH_logo_official.jpg
   :height: 240px
   :width: 240 px
   :alt: A-SLOTH logo
   :align: left

The semi-analytical model A-SLOTH (Ancient Stars and Local Observables by Tracing Halos) is the first public code that connects the formation of the first stars and galaxies to observables.
The model is based on dark matter merger trees that can either be generated based on Extended Press-Schechter theory or that can be imported from dark matter simulations.
On top of these merger trees, A-SLOTH applies analytical recipes for baryonic physics to model the formation of both metal-free and metal-poor stars and the transition between.
A-SLOTH samples individual stars and includes radiative, chemical, and mechanical feedback. It is calibrated based on six observables, such as the optical depth to Thomson scattering, the stellar mass of the Milky Way and its satellite galaxies, the number of extremely-metal poor stars, and the cosmic star formation rate density at high redshift.
A-SLOTH has versatile applications with moderate computational requirements. It can be used to constrain the properties of the first stars and high-z galaxies based on local observables, predicts properties of the oldest and most metal-poor stars in the Milky Way, can serve as a subgrid model for larger cosmological simulations, and predicts next-generation observables of the early Universe, such as supernova rates or gravitational wave events. More details on the astrophysical models can be found in our `ApJ paper <https://ui.adsabs.harvard.edu/abs/2022arXiv220600223H/abstract>`_.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

.. toctree::
   getting_started.md
   How-Tos/index.rst
   references.md
   code_doc/index.rst
   usage_policy.md
   help.md

